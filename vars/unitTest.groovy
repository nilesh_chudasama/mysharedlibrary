def call(){
  withSonarQubeEnv(credentialsId: 'sonar-jenkins') {
    sh 'mvn clean package sonar:sonar'
  }
}
