def call(String package_manager) {
    pipeline {
        agent any
        stages{
            stage('Build'){
                steps{
                    buildPackage(package_manager)
                    sh 'mvn package'
                }
            }
            stage('Unit Test'){
                steps{
                    test()
                }
            }
            stage('Deploy'){
                when{
                    expression { BRANCH_NAME ==~ /(main|master|qa|development)/ }
                }
                steps{
                    script{
                        if(env.BRANCH_NAME == 'main'){
                            deploy('production')
                        }
                        if(env.BRANCH_NAME == 'master'){
                            deploy('production')
                        }
                        if(env.BRANCH_NAME == 'qa'){
                            deploy('qa')
                        }
                        if(env.BRANCH_NAME == 'development'){
                            deploy('development')
                        }
                    }
                }
            }
        }
    }
}
