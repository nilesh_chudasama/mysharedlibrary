def call(String package_manager){
    def version
    def artifactId
    def commit_id = sh(returnStdout: true, script: "git show -s --format=%h").trim()
    String configFile = "package.json"
	def configFileExists = fileExists configFile
    if(configFileExists){
        (version, artifactId) = getBuildParameter(package_manager)
    }
    else {
        sh "echo package.json not exist"
    }
    
    sh "echo build ${artifactId}:${version}-${commit_id}"
}

def getBuildParameter(package_manager){
    if (package_manager=="mvn"){
        def pom = readMavenPom file: 'pom.xml'
        return [pom.version, pom.artifactId]
    }
    if (package_manager=="npm"){
        def props = readJSON file : 'package.json'
        return [props.version, props.name]
    }
}